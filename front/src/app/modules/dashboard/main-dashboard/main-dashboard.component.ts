import { Component, OnInit } from '@angular/core';
import { UserProviderService } from 'src/app/core/services/user-provider.service';
import { ManageConferenceApiService } from 'src/app/core/http/manage-conference-api.service';
import { LanguageService } from 'src/app/shared/services/user/language.service';
import { ArticlesApiService } from 'src/app/core/http/articles-api.service';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.scss']
})
export class MainDashboardComponent implements OnInit {

  private conference;
  private lang;
  private newRegistered = [];
  private articles = [];
  private loading;
  private lastComments = [];
  private allArticles = [];

  constructor(
    private userProvider: UserProviderService,
    private conferenceApi: ManageConferenceApiService,
    private languageService: LanguageService,
    private articlesApi: ArticlesApiService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.languageService.currentLang.subscribe(value => {
      this.lang = value;
    });
    this.conferenceApi.getConference().subscribe(res => {
      this.conference = res.conference;
      console.log(res);
    });
    this.conferenceApi.getRegisteredUsers().subscribe(res => {
      console.log(res);
      this.newRegistered = res.conferenceUsers.slice(-3);
    });
    const obj = {
        user_id: this.userProvider.getUser().id
    };

    this.articlesApi.getUserArticles(obj).subscribe(
        (res) => {
            this.articles = res.conferenceArticles;
            res.conferenceArticles.forEach(element => {
              this.lastComments.concat(element.article_comments);
            });
            this.lastComments.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1);
            this.lastComments.splice(0, 3);
        },
        () => {
        },
        () => {
          this.loading = false;
        }
    );

    this.articlesApi.getAllConferenceArticles().subscribe(res => {
      console.log(res);
      this.allArticles = res.conferenceArticles.splice(-3);
    });
  }

}
